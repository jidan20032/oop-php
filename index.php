<?php

require_once ("Animal.php");
require_once ("Frog.php");
require_once ("ape.php");

$sheep = new Animal ("shaun");
    echo "Nama : ". $sheep->name . "<br>"; // "shaun" 
    echo "Legs : ". $sheep->legs. "<br>"; // 4
    echo "Cold Blooded : ". $sheep->coldBlooded. "<br><br>"; // "no"

$kodok = new Frog("buduk");
    echo "Nama : ". $kodok->name . "<br>";
    echo "Legs : ". $kodok->legs . "<br>";
    echo "Cold Blooded : ". $kodok->coldBlooded . "<br>";
    echo "Jump : " . $kodok->jump . "<br><br>" ; // "hop hop" output akhir

$sungokong = new ape("kera sakti");
    echo "Nama : ". $sungokong->name. "<br>";
    echo "Legs : ". $sungokong->legs. "<br>";
    echo "Cold Blooded : ". $sungokong->coldBlooded. "<br>";
    echo "Yell : " . $sungokong->yell . "<br>"; // "Auooo"   

?>